function factorial(n){
	if(n === 0){
		return 1
	}
	return n * factorial(n-1)
}

var test = factorial(0)
var test1 = factorial(1)
var test2 = factorial(2)
var test3 = factorial(3)
var test4 = factorial(4)
var test5 = factorial(5)

console.log(test, test1, test2, test3, test4, test5)