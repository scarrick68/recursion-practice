// These functions would be more robust by validating
// the parameters and returning an error message when
// bad parameters are passed in.

function elfish(str, index, xish){
	if(index === undefined){
		index = 0
	}
	if(!xish){
		xish = {}
	}
	if(!str){
		return false
	}
	var currentLetter = str[index].toLowerCase()
	// console.log('currentLetter', currentLetter, index)
	if(currentLetter === "e"){
		xish[currentLetter] = "e"
	}
	if(currentLetter === "l"){
		xish[currentLetter] = "l"
	}
	if(currentLetter === "f"){
		xish[currentLetter] = "f"
	}
	if(index === (str.length - 1)){
		if(Object.keys(xish).length === 3){
			return true
		}
		return false
	}
	return elfish(str, index+1, xish)
}

// Thought I could reduce the lines of code required for a more
// elegant solution with less "ifs", but it's mostly the same.
// index can optionally be supplied to check for elfishness in
// a substring starting at the given index
function shortElf(str, index, xish){
	if(index === undefined){
		index = 0
	}
	if(!xish){
		xish = {}
	}
	if(index < str.length){
		var currentLetter = str[index].toLowerCase()
		// console.log('currentLetter', currentLetter, index)
		if(currentLetter === "e" || currentLetter === "l" || currentLetter === "f"){
			xish[currentLetter] = true
		}
		return elfish(str, index+1, xish)
	}
	else{
		if(Object.keys(xish).length === 3){
			return true
		}
		return false
	}
}

// Wrote this assuming that you just want to check for the
// existence of each letter in str1 in str2. Does not account
// for repeated letter. elf and eeeeeelf will both return true
// when compared with meatloaf. This can be solved with a bit
// more logic and using a hash table with separate chaining
// to track repeated letters. JS object literals with an array
// as a value like {f:f, e: ['e','e']} would work.

function xish(str1, str2, index, str1obj, found){
	if(index === undefined){
		index = 0
	}
	if(!str1obj){
		str1obj = {}
		for(var i = 0; i < str1.length; i++){
			var letter = str1[i].toLowerCase()
			str1obj[letter] = letter
		}
	}
	if(!found){
		found = {}
	}
	if(index < str2.length){
		if(str1obj.hasOwnProperty(str2[index].toLowerCase())){
			var currentLetter = str2[index].toLowerCase()
			found[currentLetter] = currentLetter
		}
		return xish(str1, str2, index+1, str1obj, found)
	}
	else{
		if(Object.keys(found).length === str1.length){
			return true
		}
		return false
	}
}

console.log(elfish("cat"))
console.log(elfish(""))
console.log(elfish("dog"))
console.log(elfish("elfish"))
console.log(elfish("whiteleaf"))
console.log(elfish("meatloaf"))
console.log(elfish("MEATLOAF"))

console.log("testing short elfish function")

console.log(shortElf("cat"))
console.log(shortElf(""))
console.log(shortElf("dog"))
console.log(shortElf("shortElf"))
console.log(shortElf("whiteleaf"))
console.log(shortElf("meatloaf"))
console.log(shortElf("MEATLOAF"))

console.log('testing xish function')

console.log(xish("elf","cat"))
console.log(xish("elf",""))
console.log(xish("elf","dog"))
console.log(xish("elf","xish"))
console.log(xish("elf","whiteleaf"))
console.log(xish("elf","meatloaf"))
console.log(xish("elf","MEATLOAF"))