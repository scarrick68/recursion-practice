function sumNaturalNums(n){
	if(n === 1){
		return 1
	}
	return n + sumNaturalNums(n-1)
}

var test = sumNaturalNums(5)
var test1 = sumNaturalNums(10)
var test2 = sumNaturalNums(15)
var test3 = sumNaturalNums(20)
var test4 = sumNaturalNums(25)

console.log(test, test1, test2, test3, test4)