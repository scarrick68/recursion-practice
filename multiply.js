// A recursive multiplication function which can only use the + or - operations
function multiply(a, b){
	if((a < 0 && b < 0) || (a < 0 && b > 0)){
		a = a * -1
		b = b * -1
	}
	if(a > 0){
		if(a === 1){
			return b
		}
		return b + multiply(a-1, b)
	}
	else{
		return 0
	}
}

// this works for two positive numbers. does not handle negatives rn.

// testing two positive numbers
var test = multiply(5, 5)
var test1 = multiply(5, 10)
var test2 = multiply(5, 15)
var test3 = multiply(5, 20)
var test4 = multiply(5, 25)

console.log(test, test1, test2, test3, test4)

// testing one positive and one negative number
var test = multiply(-5, 5)
var test1 = multiply(5, -10)
var test2 = multiply(-5, 15)
var test3 = multiply(5, -20)
var test4 = multiply(-5, 25)

console.log(test, test1, test2, test3, test4)

// testing two negative numbers
var test = multiply(-5, -5)
var test1 = multiply(-5, -10)
var test2 = multiply(-5, -15)
var test3 = multiply(-5, -20)
var test4 = multiply(-5, -25)

console.log(test, test1, test2, test3, test4)

// test multiplying by 0
var test = multiply(0, -25)
var test1 = multiply(-5, 0)

console.log(test, test1)
