function reverse(str, offset){
	if(!Array.isArray(str)){
		str = str.split(" ")
		if(str.length === 0 || str.length === 1){
			return str.join()
		}
	}
	if(offset === undefined){
		offset = 0
	}
	if(offset === Math.ceil(str.length / 2)){
		return str.join(" ")
	}
	var temp = str[offset]
	str[offset] = str[str.length - offset - 1]
	str[str.length - offset - 1] = temp
	return reverse(str, offset + 1)
}

var str = ""
var str1 = "cat"
var str2 = "the cat is running"
var str3 = "rockets fly fast"

console.log("original:", str,  "reversed:",  reverse(str))
console.log("original:", str1, "reversed:",  reverse(str1))
console.log("original:", str2, "reversed:",  reverse(str2))
console.log("original:", str3, "reversed:",  reverse(str3))
