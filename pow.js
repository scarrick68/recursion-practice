// take a double to a power, the power can also be negative. function must be recursive.
function pow(num, power){
	if(power >= 0){
		if(power === 0){
			return 1
		}
		return num * pow(num, power - 1)
	}
	if(power <= 0){
		if(power === 0){
			return 1
		}
		return 1 / (num * pow(num, -1 * (power + 1)))
	}
}

// testing positive exponent
var test = pow(4, 3)
var test1 = pow(2, 5)
var test2 = pow(5, 2)

console.log(test, test1, test2)

// testing negative exponent
var test = pow(4, -3)
var test1 = pow(2, -5)
var test2 = pow(5, -2)

console.log(test, test1, test2)

// testing exponent is 0
var test = pow(4, 0)
var test1 = pow(2, 0)
var test2 = pow(5, 0)

console.log(test, test1, test2)