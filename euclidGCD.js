function gcd(dividend, divisor){
	if(dividend < 0){
		dividend = dividend * -1
	}
	if(divisor < 0){
		divisor = divisor * -1
	}
	if(divisor > dividend){
		var temp = dividend
		dividend = divisor
		divisor = temp
	}
	if(divisor === 0){
		return "cannot divide by "
	}
	if(dividend === 0){
		return 0
	}
	var remainder = dividend % divisor
	dividend = parseInt(dividend / divisor)
	if(remainder === 0){
		return divisor
	}
	return gcd(divisor, remainder)
}

var test = gcd(30, 6)
var test1 = gcd(32, 5)
var test2 = gcd(108, 30)
var test3 = gcd(16, 12)
var test4 = gcd(72, 48)
var test5 = gcd(64, 48)
var test6 = gcd(48, 64)
var test7 = gcd(5, 32)
var test8 = gcd(-108, 30)
var test9 = gcd(108, -30)
var test10 = gcd(-108, -30)

console.log(test, test1, test2, test3, test4, test5, test6, test7, test8, test9, test10)